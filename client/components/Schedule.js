import React from 'react';
import PropTypes from 'prop-types';
import { FlatButton } from 'material-ui';
import { TableRow, TableRowColumn } from 'material-ui/Table';

export default class ClassName extends React.Component {
  static propTypes = {
    removeSchedule: PropTypes.func.isRequired,
    schedule: PropTypes.shape({
      id: PropTypes.number,
      end: PropTypes.number,
      start: PropTypes.number,
    }).isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  onDelete = () => {
    this.props.removeSchedule(this.props.schedule.id);
  }

  render() {
    const { start, end } = this.props.schedule;
    return (
      <TableRow selected={false}>
        <TableRowColumn style={{ textAlign: 'center' }}>
          {new Date(start).toLocaleString()}
        </TableRowColumn>
        <TableRowColumn style={{ textAlign: 'center' }} >
          {new Date(end).toLocaleString()}
        </TableRowColumn>
        <TableRowColumn style={{ textAlign: 'center' }}>
          <FlatButton label="Delete" secondary onTouchTap={this.onDelete} />
        </TableRowColumn>
      </TableRow>
    );
  }
}
