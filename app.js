import Koa from 'koa';
import webpack from 'webpack';
import serve from 'koa-static';
import convert from 'koa-convert';
import { MongoClient } from 'mongodb';
import bodyParser from 'koa-bodyparser';
import webpackDev from 'koa-webpack-dev-middleware';
import webpackHot from 'koa-webpack-hot-middleware';
import config from './config';
import routes from './routes';
import devConfig from './webpack.dev.config';
import './crontab';

// de tranh phai cau hinh cron tren may chu nen em dung cron tam
// qua thu vien de chuong trinh co the chay luon

(async () => {
  try {
    const app = new Koa();

    if (process.env.NODE_ENV !== 'production') {
      const compile = webpack(devConfig);
      app.use(convert(webpackDev(compile, {
        hot: true,
        stats: { colors: true },
      })));
      app.use(convert(webpackHot(compile)));
    }

    app.context.config = config;
    app.context.db = await MongoClient.connect(config.db);

    app.use(bodyParser());
    app.use(serve('public'));
    app.use(routes.routes());
    app.use(routes.allowedMethods());

    app.listen(config.port);
    console.log(`✅ Server is running on : http://localhost:${config.port}`);
  } catch (e) { console.error(e); }
})();
