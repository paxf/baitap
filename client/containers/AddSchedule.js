import { connect } from 'react-redux';
import AddSchedule from './../components/AddSchedule';
import { addSchedule } from './../actions';

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
  addSchedule: (Schedule) => { dispatch(addSchedule(Schedule)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(AddSchedule);
