import React from 'react';
import PropTypes from 'prop-types';
import { Paper, FlatButton } from 'material-ui';
import { Table, TableRow, TableBody, TableHeader, TableRowColumn, TableHeaderColumn } from 'material-ui/Table';
import IconSearch from 'material-ui/svg-icons/action/search';
import Schedule from './../containers/Schedule';
import AddSchedule from './../containers/AddSchedule';

export default class App extends React.Component {
  static propTypes = {
    calMatch: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    schedules: PropTypes.array.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  findMatch = () => {
    this.props.calMatch(this.props.schedules);
  }

  render() {
    const match = (Object.keys(this.props.match).length === 0) ? '' : (
      <Paper zDepth={1} style={{ marginTop: 50 }}>
        <Table selectable={false} multiSelectable={false} >
          <TableBody displayRowCheckbox={false} >
            <TableRow>
              <TableRowColumn>{this.props.match.homeTeamName}</TableRowColumn>
              <TableRowColumn>{this.props.match.awayTeamName}</TableRowColumn>
              <TableRowColumn>
                {this.props.match.date && new Date(this.props.match.date).toLocaleString()}
              </TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );

    return (
      <div style={{ margin: '50px auto', width: '700px' }}>
        <Paper zDepth={1}>
          <Table selectable={false} multiSelectable={false} >
            <TableHeader displaySelectAll={false} adjustForCheckbox={false} >
              <AddSchedule />
              <TableRow>
                <TableHeaderColumn style={{ textAlign: 'center' }} tooltip="Start">Start</TableHeaderColumn>
                <TableHeaderColumn style={{ textAlign: 'center' }} tooltip="End">End</TableHeaderColumn>
                <TableHeaderColumn style={{ textAlign: 'center' }} tooltip="Delete">Delete</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false} >
              {this.props.schedules.map((schedule) => (
                <Schedule key={schedule.id} schedule={schedule} />
              ))}
              <TableRow>
                <TableRowColumn colSpan="3" style={{ textAlign: 'center' }} >
                  <FlatButton label="Find Match" primary onTouchTap={this.findMatch} icon={<IconSearch />} />
                </TableRowColumn>
              </TableRow>
            </TableBody>
          </Table>
        </Paper>

        {match}

      </div>
    );
  }
}
