import React from 'react';
import thunk from 'redux-thunk';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { MuiThemeProvider, getMuiTheme } from 'material-ui/styles';
import { blueGrey500, blueGrey700, blueGrey900 } from 'material-ui/styles/colors';
import { logger } from 'redux-logger';
import App from './containers/App';
import Reducers from './reducers';

injectTapEventPlugin();

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: blueGrey500,
    primary2Color: blueGrey700,
    primary3Color: blueGrey900,
    pickerHeaderColor: blueGrey700,
  },
  appBar: { height: 50 },
});

const store = createStore(Reducers, applyMiddleware(thunk, logger));

render(
  <MuiThemeProvider muiTheme={muiTheme} >
    <Provider store={store}>
      <App />
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root'),
);
