import { connect } from 'react-redux';
import Schedule from './../components/Schedule';
import { removeSchedule } from './../actions';

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
  removeSchedule: (id) => { dispatch(removeSchedule(id)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(Schedule);
