import React from 'react';
import PropTypes from 'prop-types';
import { DatePicker, TimePicker, FlatButton } from 'material-ui';
import { TableRow, TableHeaderColumn } from 'material-ui/Table';

function disableWeekday(date) {
  const now = new Date();
  return (date.getTime() < now.getTime())
    || (date.getTime() > new Date().setDate(now.getDate() + 7))
    || (date.getDay() > 0 && date.getDay() < 6);
}

export default class ClassName extends React.Component {
  static propTypes = {
    addSchedule: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      date: null,
      end_time: null,
      start_time: null,
    };
  }

  onChangeDate = (_, date) => {
    this.setState({ date });
  }

  onChangeStartTime = (_, date) => {
    this.setState({ start_time: date });
  }

  onChangeEndTime = (_, date) => {
    this.setState({ end_time: date });
  }

  addSchedule = () => {
    if (!this.state.date || !this.state.start_time || !this.state.end_time) {
      return;
    }

    const date = new Date(this.state.date);
    let start = new Date(this.state.start_time);
    let end = new Date(this.state.end_time);

    if (start.getTime() >= end.getTime()) {
      return;
    }

    start = date.setHours(start.getHours(), start.getMinutes(), 0, 0);
    end = date.setHours(end.getHours(), end.getMinutes(), 0, 0);
    this.props.addSchedule({ start, end });
  }

  render() {
    return (
      <TableRow selected={false}>
        <TableHeaderColumn style={{ textAlign: 'center' }}>
          <DatePicker
            fullWidth
            hintText="date"
            value={this.state.date}
            onChange={this.onChangeDate}
            shouldDisableDate={disableWeekday}
          />
        </TableHeaderColumn>
        <TableHeaderColumn style={{ textAlign: 'center' }}>
          <TimePicker fullWidth hintText="start" format="24hr" onChange={this.onChangeStartTime} />
          <TimePicker fullWidth hintText="end" format="24hr" onChange={this.onChangeEndTime} />
        </TableHeaderColumn>
        <TableHeaderColumn style={{ textAlign: 'center' }}>
          <FlatButton label="Add" primary onTouchTap={this.addSchedule} />
        </TableHeaderColumn>
      </TableRow>
    );
  }
}
