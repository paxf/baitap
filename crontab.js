import Axios from 'axios';
import { CronJob } from 'cron';
import { MongoClient } from 'mongodb';
import config from './config';

const axios = Axios.create({
  baseURL: 'http://api.football-data.org/v1/',
  timeout: 1000,
  // headers: { 'X-Auth-Token': 'cdd2fa36be9c429e90bcf680f59a1a1a' },
  headers: { 'X-Auth-Token': '7d6fc0b8f91044799803bee16a8c535e' },
});

const cron = () => {
  // const job = new CronJob('*/5 * * * *', async () => { // cron every 5 minutes
  const job = new CronJob('00 00 * * * *', async () => { // cron every day
    try {
      const db = await MongoClient.connect(config.db);

      const teams = await axios.get('http://api.football-data.org/v1/soccerseasons/426/teams')
                          .then(res => res.data.teams);

      console.dir(teams, { depth: null, colors: true });
      await Promise.all(teams.map(team => db.collection('teams').update({
        code: team.code,
      }, {
        $set: { code: team.code,
          name: team.name,
          crestUrl: team.crestUrl,
          shortName: team.shortName,
          href: team._links.self.href,
          squadMarketValue: team.squadMarketValue,
        },
      }, { upsert: true })));

      const standing = await axios.get('http://api.football-data.org/v1/soccerseasons/426/leagueTable')
                              .then(res => res.data.standing);

      console.dir(standing, { depth: null, colors: true });
      await Promise.all(standing.map(league => db.collection('leagueTable').update({
        href: league._links.team.href,
      }, {
        $set: {
          href: league._links.team.href,
          away: league.away,
          home: league.home,
          wins: league.wins,
          draws: league.draws,
          goals: league.goals,
          losses: league.losses,
          points: league.points,
          crestUrl: league.crestURI,
          position: league.position,
          teamName: league.teamName,
          playedGames: league.playedGames,
          goalsAgainst: league.goalsAgainst,
          goalDifference: league.goalDifference,
        },
      }, { upsert: true })));

      const fixtures = await axios.get('http://api.football-data.org/v1/soccerseasons/426/fixtures')
                              .then(res => res.data.fixtures);


      console.dir(fixtures, { depth: null, colors: true });
      await Promise.all(fixtures.map(fixture => db.collection('fixtures').update({
        date: fixture.date,
        matchday: fixture.matchday,
        self: fixture._links.self.href,
        homeTeam: fixture._links.homeTeam.href,
        awayTeam: fixture._links.awayTeam.href,
      }, {
        $set: {
          date: fixture.date,
          status: fixture.status,
          matchday: fixture.matchday,
          self: fixture._links.self.href,
          homeTeamName: fixture.homeTeamName,
          awayTeamName: fixture.awayTeamName,
          homeTeam: fixture._links.homeTeam.href,
          awayTeam: fixture._links.awayTeam.href,
        },
      }, { upsert: true })));
    } catch (e) {
      console.log('error', e);
    }
  }, null, true, 'America/Los_Angeles');

  job.start();
};

cron();
