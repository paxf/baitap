import { connect } from 'react-redux';
import App from './../components/App';
import { calMatch } from './../actions';

const mapStateToProps = (state) => ({
  match: state.match,
  schedules: state.schedules,
});

const mapDispatchToProps = (dispatch) => ({
  calMatch: (schedules) => dispatch(calMatch(schedules)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
