import { SET_MATCH } from './../actions';

const Schedule = (state = {}, action) => {
  switch (action.type) {
    case SET_MATCH:
      return action.match;
    default:
      return state;
  }
};

export default Schedule;
