const config = {
  port: process.env.PORT || 8080,
  db: process.env.DATABASE_URI || 'mongodb://localhost:27017/premierleague',
};

export default config;
