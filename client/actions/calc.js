import Axios from 'axios';

export const SET_MATCH = 'SET_MATCH';

export const calMatch = (schedules) => async (dispatch) => {
  try {
    const res = await Axios.post('/calc', { schedules });

    dispatch({ type: SET_MATCH, match: res.data });
  } catch (e) {
    dispatch({ type: SET_MATCH, match: {} });
    console.log(e);
  }
};
