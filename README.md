môi trường phát triển ubuntu.

###Installation
```
$ [sudo] npm i -g webpack babel-cli
$ npm i
```

### Run
```
$ npm run start
$ open http://localhost:8080
```

### Run Dev
```
$ npm run dev
$ open http://localhost:8080
```

### Docs

vì một số lý cần phải setup crontab trên máy nên em dùng tạm thư viện để cronjob (em đang để cron là mỗi ngày một lần). Anh chạy thử lần đầu thì hãy sửa lại thời gian cron để có dữ liệu để test.
