export const ADD_SCHEDULE = 'ADD_SCHEDULE';
export const REMOVE_SCHEDULE = 'REMOVE_SCHEDULE';

export const addSchedule = (schedule) => (dispatch) => {
  dispatch({ type: ADD_SCHEDULE, schedule });
};

export const removeSchedule = (id) => (dispatch) => {
  dispatch({ type: REMOVE_SCHEDULE, id });
};
