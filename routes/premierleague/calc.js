function nextSatDay() {
  const time = new Date(new Date().setHours(0, 0, 0, 0));

  let plus = 1;

  switch (time.getDay()) {
    case 0:
      plus = 6; break;
    case 1:
      plus = 5; break;
    case 2:
      plus = 4; break;
    case 3:
      plus = 3; break;
    case 4:
      plus = 2; break;
    case 5:
    case 6:
    default:
      plus = 1; break;
  }

  return new Date(time.setDate((time.getDate() + plus)));
}

export default class CalcMatch {
  constructor(schedules, premierleague) {
    this.schedules = schedules;
    this.premierleague = premierleague;
  }

  calc() {
    let start = nextSatDay();
    const end = new Date(start).setDate((start.getDate() + 2));
    start = start.getTime();

    const premierleague = this.premierleague
      .filter(premier => {
        const date = new Date(premier.date).getTime();
        return (date > start) && (date < end);
      })
      .map(e => {
        let matchStart = new Date(e.date);
        const matchEnd = new Date(e.date).setHours(matchStart.getHours() + 2);
        matchStart = matchStart.getTime();

        let rank = 0;

        this.schedules.forEach(schedule => {
          if (schedule.start <= matchStart && matchEnd <= schedule.end) {
            rank += 1;
          } else if (schedule.start > matchEnd || matchStart > schedule.end) {
            rank += 0;
          } else {
            rank += 0.25;
          }
        });

        return { ...e, rank };
      }).sort((a, b) => (a.rank < b.rank));

    if (!premierleague.length) {
      return [];
    }

    const maxRank = premierleague[0].rank;
    return premierleague.filter(e => e.rank === maxRank);
  }
}
