import fs from 'fs';
import Router from 'koa-router';
import premierleague from './premierleague';

const router = new Router();

router.get('/test', async (ctx) => {
  ctx.body = 'successfully';
});

router.post('/calc', premierleague.checkValid, premierleague.calcMatch);

router.get('*', async (ctx) => {
  ctx.type = 'html';
  ctx.body = await fs.createReadStream('public/index.html');
});

export default router;
