import CalcMatch from './calc';

const checkValid = async (ctx, next) => {
  const { schedules } = ctx.request.body;

  if (!(schedules instanceof Array)) {
    ctx.status = 400;
    ctx.body = { message: 'INVALID_DATA' };
    return;
  }

  const findInvalid = schedules.find(schedule => (
    new Date(schedule.start).getTime() >= new Date(schedule.end).getTime()
  ));

  if (findInvalid) {
    ctx.status = 400;
    ctx.body = { message: 'INVALID_DATA' };
    return;
  }

  await next();
};

const calcMatch = async (ctx) => {
  const { schedules } = ctx.request.body;

  const premierleague = await ctx.db.collection('fixtures').find({
    status: { $ne: 'FINISHED' },
  }).toArray();

  const calc = new CalcMatch(schedules, premierleague);
  let matches = calc.calc();

  if (!matches.length) {
    ctx.body = {}; return;
  }

  matches = await Promise.all(matches.map(async match => {
    const teams = await ctx.db.collection('leagueTable').find({
      href: {
        $in: [match.homeTeam, match.awayTeam],
      },
    }).toArray();

    const points = teams[0].points + teams[1].points;
    return { ...match, points };
  }));

  matches = matches.sort((a, b) => a.points < b.points);
  ctx.body = matches[0];
};

export default {
  calcMatch,
  checkValid,
};
