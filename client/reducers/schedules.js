import { ADD_SCHEDULE, REMOVE_SCHEDULE } from './../actions';

let id = 1;

const Schedule = (state = [], action) => {
  switch (action.type) {
    case ADD_SCHEDULE:
      id += 1;
      return state.concat({ id, ...action.schedule });
    case REMOVE_SCHEDULE:
      return state.filter(schedule => (schedule.id !== action.id));
    default:
      return state;
  }
};

export default Schedule;
