import { combineReducers } from 'redux';
import match from './match';
import schedules from './schedules';

export default combineReducers({
  match,
  schedules,
});
