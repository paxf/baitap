const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractSass = new ExtractTextPlugin({
  // filename: '[name].[contenthash].css',
  filename: 'styles/main.css',
});

module.exports = {
  entry: [
    'eventsource-polyfill',
    './client/index.js',
  ],
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js',
  },
  resolve: {
    modules: ['node_modules', path.resolve(__dirname, 'client')],
    extensions: ['.js', '.json', '.jsx', '.sass', '.scss', '.css'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: [path.resolve(__dirname, 'client')],
        loader: 'babel-loader',
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({
          use: [
            { loader: 'css-loader' },
            { loader: 'sass-loader' },
          ],
          fallback: 'style-loader',
        }),
      },
      {
        test: /\.html$/,
        use: ['htmllint-loader', { loader: 'html-loader', options: { /* ... */ } }],
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
      },
    }),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compress: { warnings: false },
      output: { comments: false },
    }),
    extractSass,
    new webpack.optimize.OccurrenceOrderPlugin(),
  ],
};
